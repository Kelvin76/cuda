# CNN with CUDA

This program is a practice to improve the performance of convolutional neural networks (CNN) using GPU with CUDA to do some of the work.

## Introduction of the program

The input files consist of a CNN file describing the CNN and an input file containing a number of input matrices.
The output files contain the output vectors for each input matrix.

To run the code, use:

    cargo run --release -- <mode> <cnn_file> <input_file> <output_file>

where <mode> is either "cpu" "cuda". All the files are pathnames (unlike in
the earlier Sudoku example).  You would typically use the following commands:

    cargo run --release -- cpu input/cnn.csv input/in.csv output/out.csv

    cargo run --release -- cuda input/cnn.csv input/in.csv output/out_cuda.csv

The program outputs the time spent doing "actual work", which is the
work of converting input matrices to output vectors.  This measurement
does not include I/O or the overhead of initializing the GPU. As such,
the time should be lower for the CUDA version than the CPU version.

The repo also includes 2 helper scripts, written in Python:

  generate.py generates random CNNs and input matrices, defaulting to
     "input/cnn.csv" and "input/in.csv"

  compare.py compares 2 output matrices to see if they are "close enough".
      Used to test the implementations for correctness.
      Reads output/out.csv and output/out_cuda.csv

You can run each of them using the python3 command (e.g. "python3 compare.py").


# Implementing CUDA for CNN to improve performance

## Summary

The utilization of CUDA focused on implementing the files `src/cuda.rs` and `kernel/kernel.cu`, which added support for utilizing CUDA for the Convoultional Neural Network. This should improve the performance of the program by a certain amount. The code in `src/cuda.rs` is the host code that will run on the cpu. The code in `kernel/kernel.cu` is the CUDA code that will be executed on the GPU. The host code will call the CUDA code and pass the necessary parameters to it and then gather the results.

## Technical Details

Initialization(`init()` from `cuda.rs`):
- At the beginning, `main.rs` will read in a CNN file and generate a `Cnn` object. Then it will use the `Cnn` object as an argument to call the function `cuda::CudaContext::init()` in order to initialize a `CudaContect` object. This object contains the CNN object and the CUDA context. After that, the system can call the function `cuda::CudaContext::compute()` to run the CNN on the GPU.

Computation process (`compute()` from `cuda.rs`):
- The `cuda::CudaContext::compute()` function will take in 2 arguments. One is a `CudaContext`, which contains the CNN information. The other one is the input image represented as a 100x100 matrix. There are 3 general steps for each convolution matrix in the CNN, and we have 10 convolution matrices in total. The 3 steps are:
1. Do convolution on the input image with each CNN convolution matrix. The input image is assumed to have 100x100 pixels. The convolution matrices are 5x5. After convolution, the output matrix will have 20x20 elements.
2. Filter the output matrix with ReLU. This step will set all negative values to 0.
3. Do dot product between the filtered output matrix and a weight matrix. The output of this step will be a single value.
Since we have 10 convolution matrices, we will have our final output to be a vector of 10 values. The function `compute()` will return this vector.

Kernel code (`kernel.cu`):
- The steps mentioned in the computation process are only about the host code. However, each step in the host code will also call a different kernel function written in `kernel.cu`, i.e., the CUDA code. The kernel functions for each step are:
1. `convolution_layer`: this kernel function will do convolution on the input image with every convolution matrix. That is, it will do convolution between the convolution matrix with every 5x5 matrix in the 100x100 input image. The output of this kernel function will be a 20x20x10 matrix, where the 3rd dimension is the 10 convolution matrices. *When calling from the host code*, we will give it a grid size of 10, and a block size of 20x20. That is, we will have 10 convolution matrices, each of will have 20x20 elements.
2. `ReLU_layer`: this kernel function will filter the output of the previous kernel function with ReLU. That is, it will set all negative values to 0. The output of this kernel function is the same as its input, i.e., a 20x20x10 matrix. *When calling from the host code*, we will give it a grid size of 10, and a block size of 20x20. Same as the previous kernel function.
3. `output_layer`: this kernel function will do dot product between the output of the ReLU function and the weight matrix. The output of this kernel function is a 10x1 matrix, which is the final output of the CNN. *When calling from the host code*, we will give it a grid size of 10, and a block size of 200. That is, for each of the 10 convolution matrices, we will come up with 200 intermediate sums. Then we will use atomic addition to add all the intermediate sums together to get the final value for each block.

## Testing

We are being provided with 2 files: `generate.py` and `compare.py`. One is to generate different input "image", the other is to compare 2 results. What I did is to write a script `correctness_test_script.py` to generate 20 different input files. Then execute the original (cpu) implementation and my (cuda) implementation on each of them. At the end, I used `compare.py` to compare the results. Since all of the 20 results are the same, I can conclude that my implementation is correct.


## Performance

Similar to the testing strategy, I wrote a script `performance_test_script.py` to compare the performance of my implementation with the original implementation. I used `hyperfine` to benchmark the original implementation and my implementation for 10 different input files. Some results are shown below if you are interested. We can see that the CUDA version is in fact slower. The possible reasons for the slow CUDA version are the following.
1. Each launch (kernel call) does very little work (the most work is around 40,000 elements).
2. The operation per launch (kernel call) is also very simple. Just addition and multiplication, so it's not really a computationally intensive problem.  
Therefore, the overhead of starting up and transferring data between the CPU and GPU is likely to outweigh the benefits of using CUDA. As a result, the CUDA version is a bit slower here.

| Test # | Original CPU Implementation | My CUDA Implementation | Slow Down |
| ------ | --------------------------- | ---------------------- | --------- |
| 1      | 219 ms                      | 344 ms                 | ~1.57     |
| 2      | 219 ms                      | 359 ms                 | ~1.64     |
| 3      | 205 ms                      | 344 ms                 | ~1.68     |
| 4      | 202 ms                      | 356 ms                 | ~1.76     |
| 5      | 212 ms                      | 371 ms                 | ~1.75     |

