#!/usr/bin/python
import os
from subprocess import call
from generate import generate
from compare import compare_function

try:
  from subprocess import DEVNULL
except ImportError:
  import os
  DEVNULL = open(os.devnull, 'wb')

test_count = 0

input_cnn_file = "input/cnn.csv"
input_file = "input/in.csv"
output_cpu_file = "output/out.csv"
output_cuda_file = "output/out_cuda.csv"

run_cpu_command = " ".join(["./cnn", "cpu", input_cnn_file, input_file, output_cpu_file])
run_cuda_command = " ".join(["./cnn", "cuda", input_cnn_file, input_file, output_cuda_file])

def check():
    # Generate different input files
    generate()

    # Run
    call(["hyperfine", "--warmup", "3", run_cpu_command, run_cuda_command], stderr=DEVNULL)

if __name__ == "__main__":
    # Generate file
    call(["cargo", "build", "--release"], stderr=DEVNULL)
    call(["cp", "target/release/cnn", "./cnn"], stderr=DEVNULL)
    for _ in range(10):
        check()
