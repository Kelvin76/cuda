#!/usr/bin/python
import os
from subprocess import call
from generate import generate
from compare import compare_function

try:
  from subprocess import DEVNULL
except ImportError:
  import os
  DEVNULL = open(os.devnull, 'wb')

test_count = 0

input_cnn_file = "input/cnn.csv"
input_file = "input/in.csv"
output_cpu_file = "output/out.csv"
output_cuda_file = "output/out_cuda.csv"

def run_cpu_version():
    call(["cargo", "run", "--release", "cpu", input_cnn_file, input_file, output_cpu_file], stdout=DEVNULL, stderr=DEVNULL)

def run_cuda_version():
    call(["cargo", "run", "--release", "cuda", input_cnn_file, input_file, output_cuda_file], stdout=DEVNULL, stderr=DEVNULL)


def check():
    # Generate different input files
    generate()

    # Run CPU version
    run_cpu_version()

    # Run CUDA version
    run_cuda_version()

    # Compare results
    compare_function()

if __name__ == "__main__":
    for test in range(20):
        check()
