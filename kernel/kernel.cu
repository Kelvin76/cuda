#include <stdio.h>

#define INPUT_DIM 100
#define FILTER_DIM 5
#define CONV_OUT_DIM (INPUT_DIM / FILTER_DIM)
#define CONV_LAYER_SIZE 10
#define OUT_NEURON_MAT_DIM (CONV_OUT_DIM * CONV_OUT_DIM)
#define OUT_NEURON_DIM (OUT_NEURON_MAT_DIM * CONV_LAYER_SIZE)
#define OUT_LAYER_SIZE 10
#define OUT_LAYER_THREAD_WORKLOAD 200

__device__ double atomicAdd(double* address, double val) {
    unsigned long long int* address_as_ull = (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
            __double_as_longlong(val +
            __longlong_as_double(assumed)));
        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
    } while (assumed != old);
    return __longlong_as_double(old);
}

extern "C" __global__ void convolution_layer(
    const double input_matrix[INPUT_DIM][INPUT_DIM],
    const double convol_filter[CONV_LAYER_SIZE][FILTER_DIM][FILTER_DIM],
    double convol_out[CONV_LAYER_SIZE][CONV_OUT_DIM][CONV_OUT_DIM]) {

    // Dot product matrices
    int row_base = threadIdx.x;
    int col_base = threadIdx.y;
    double prod = 0;
    for (int row = 0; row < FILTER_DIM; row++) {
        for (int col = 0; col < FILTER_DIM; col++) {
            prod += input_matrix[row_base * FILTER_DIM + row][col_base * FILTER_DIM + col] * convol_filter[blockIdx.x][row][col];
        }
    }
    convol_out[blockIdx.x][row_base][col_base] = prod;
}

extern "C" __global__ void ReLU_layer(double input[OUT_NEURON_DIM]) {
    int in_idx = blockIdx.x * OUT_NEURON_MAT_DIM + threadIdx.x;
    input[in_idx] = (input[in_idx] >= 0.0) * input[in_idx];
}

// Merged the 2 above layers into 1
extern "C" __global__ void convolution_and_ReLU_layer(
    const double input_matrix[INPUT_DIM][INPUT_DIM],
    const double convol_filter[CONV_LAYER_SIZE][FILTER_DIM][FILTER_DIM],
    double convol_out[CONV_LAYER_SIZE][CONV_OUT_DIM][CONV_OUT_DIM]) {

    // Dot product matrices
    int row_base = threadIdx.x;
    int col_base = threadIdx.y;
    double prod = 0;
    for (int row = 0; row < FILTER_DIM; row++) {
        for (int col = 0; col < FILTER_DIM; col++) {
            prod += input_matrix[row_base * FILTER_DIM + row][col_base * FILTER_DIM + col] * convol_filter[blockIdx.x][row][col];
        }
    }
    convol_out[blockIdx.x][row_base][col_base] = fmax(prod, 0.0);
}

extern "C" __global__ void output_layer(
    const double input[OUT_NEURON_DIM],
    const double weights[OUT_LAYER_SIZE][OUT_NEURON_DIM],
    double output[OUT_LAYER_SIZE]) {

    int block_idx = blockIdx.x;
    int thread_idx = threadIdx.x * OUT_LAYER_THREAD_WORKLOAD;
    double prod = 0;
    for (int i = thread_idx; i < thread_idx + OUT_LAYER_THREAD_WORKLOAD; ++i){
        prod += weights[block_idx][i] * input[i];
    }
    atomicAdd(&output[block_idx], prod);
}
