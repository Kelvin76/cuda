// This is the skeleton for the CUDA implementation

use crate::cnn::*;
use rustacuda::launch;
use rustacuda::memory::DeviceBox;
use rustacuda::prelude::*;
use std::error::Error;
use std::ffi::CString;

// Fields need to be ordered this way so the DeviceBoxes are
// dropped before the Context. Otherwise the drop will panic.

pub struct CudaContext {
    conv_layer: DeviceBox<ConvLayer>,
    output_layer: DeviceBox<OutputLayer>,
    module: Module,
    stream: Stream,
    _context: Context,
}

impl CudaContext {
    pub fn init(cnn: &Cnn) -> Result<Self, Box<dyn Error>> {
        // Set up the context
        rustacuda::init(CudaFlags::empty())?;
        let device = Device::get_device(0)?;
        let context = Context::create_and_push(
            ContextFlags::MAP_HOST | ContextFlags::SCHED_AUTO, device)?;

        // Create a stream
        let ptx = CString::new(include_str!("../kernel/kernel.ptx"))?;
        let module = Module::load_from_string(&ptx)?;
        let stream = Stream::new(StreamFlags::DEFAULT, None)?;

        // Copy the CNN to the GPU
        let conv_layer = DeviceBox::new(&cnn.conv_layer)?;
        let output_layer = DeviceBox::new(&cnn.output_layer)?;

        return Ok(Self {
            conv_layer,
            output_layer,
            module,
            stream,
            _context: context,
        });
    }

    pub fn compute(&mut self, input: &InputMatrix) -> Result<OutputVec, Box<dyn Error>> {
        let module = &self.module;
        let stream = &self.stream;

        // convolution and ReLU layer
        let conv_layer = &mut self.conv_layer;
        let mut conv_output = DeviceBuffer::from_slice(&[[[0.0; CONV_OUT_DIM]; CONV_OUT_DIM]; CONV_LAYER_SIZE]).unwrap();
        let mut input = DeviceBox::new(input)?;

        // Output layer
        let weights = &mut self.output_layer;
        let mut outputvec = DeviceBox::new(&OutputVec([0.0; OUT_LAYER_SIZE]))?;

        unsafe{

            // Merged 2 operations into 1
            /*
            // convolution layer
            launch!(module.convolution_layer<<<CONV_LAYER_SIZE as u32, (CONV_OUT_DIM as u32, CONV_OUT_DIM as u32), 0, stream>>>(
                input.as_device_ptr(),
                conv_layer.as_device_ptr(),
                conv_output.as_device_ptr()
            ))?;

            // ReLU layer
            launch!(module.ReLU_layer<<<CONV_LAYER_SIZE as u32, OUT_NEURON_MAT_DIM as u32, 0, stream>>>(
                conv_output.as_device_ptr()
            ))?;
            */

            launch!(module.convolution_and_ReLU_layer<<<CONV_LAYER_SIZE as u32, (CONV_OUT_DIM as u32, CONV_OUT_DIM as u32), 0, stream>>>(
                input.as_device_ptr(),
                conv_layer.as_device_ptr(),
                conv_output.as_device_ptr()
            ))?;

            launch!(module.output_layer<<<OUT_LAYER_SIZE as u32, OUT_LAYER_THREAD_DIM as u32, 0, stream>>>(
                conv_output.as_device_ptr(),
                weights.as_device_ptr(),
                outputvec.as_device_ptr()
            ))?;
        }

        // Synchronize the stream
        stream.synchronize()?;

        // Copy the output back to the CPU
        let mut output = OutputVec([0.0; OUT_LAYER_SIZE]);
        outputvec.copy_to(&mut output)?;

        Ok(output)
    }
}
